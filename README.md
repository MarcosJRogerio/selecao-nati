# Seleção Nati 2020

Repositório com as soluções para seleção do NATI/UNIFOR.
Linguagem: Javascript

## Desafio 1
É possível ver solução somente abrindo o arquivo html.

- [Desafio 1](https://gitlab.com/MarcosJRogerio/selecao-nati/-/blob/master/desafios-javascript/1encontre_o_maior_numero.html)

## Desafios 2 e 3
Para ter acesso a resolução destes 2 desafios é necessário abrir o console no browser.

- [Desafio 2](https://gitlab.com/MarcosJRogerio/selecao-nati/-/blob/master/desafios-javascript/2desafio_do_array_simples.html)

- [Desafio 3](https://gitlab.com/MarcosJRogerio/selecao-nati/-/blob/master/desafios-javascript/3encontre_o_produto_maximo.html)

